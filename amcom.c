#include <string.h>
#include <stdint.h>
#include <stdio.h>
#include "amcom.h"

/// Start of packet character
const uint8_t  AMCOM_SOP         = 0xA1;
const uint16_t AMCOM_INITIAL_CRC = 0xFFFF;

static uint16_t AMCOM_UpdateCRC(uint8_t byte, uint16_t crc)
{
	byte ^= (uint8_t)(crc & 0x00ff);
	byte ^= (uint8_t)(byte << 4);
	return ((((uint16_t)byte << 8) | (uint8_t)(crc >> 8)) ^ (uint8_t)(byte >> 4) ^ ((uint16_t)byte << 3));
}


void AMCOM_InitReceiver(AMCOM_Receiver* receiver, AMCOM_PacketHandler packetHandlerCallback, void* userContext) {
	// TODO
	receiver->packetHandler = packetHandlerCallback;				//add callback
	receiver->userContext = userContext;
	receiver->receivedPacketState = AMCOM_PACKET_STATE_EMPTY;
}

size_t AMCOM_Serialize(uint8_t packetType, const void* payload, size_t payloadSize, uint8_t* destinationBuffer) {
	// TODO
	/////////CRC
	uint16_t local_crc = AMCOM_INITIAL_CRC;
	local_crc = AMCOM_UpdateCRC(packetType, local_crc);
	local_crc = AMCOM_UpdateCRC(payloadSize, local_crc);
	for (size_t k = 0; k< payloadSize; k++){
		local_crc = AMCOM_UpdateCRC(((char*)payload)[k],local_crc);
	}
	///////Packet Serialization
	uint8_t n = 0;						//counter
	//SOP
	*destinationBuffer = AMCOM_SOP; 
	n++;

	//TYPE
	*(destinationBuffer+n) = packetType;
	n++;

	//LENGTH
	*(destinationBuffer+n) = payloadSize;
	n++;

	//CRC
	*(destinationBuffer+n) = local_crc & 0xff;
	n++;
	*(destinationBuffer+n) = local_crc >> 8;
	n++;

	//PAYLOAD
	for (size_t k = 0; k< payloadSize; k++){
		*(destinationBuffer+n) = ((char*)payload)[k];
		n++;
	}

	if (n > 205) return 0;
	
	return n;

}

void AMCOM_Deserialize(AMCOM_Receiver* receiver, const void* data, size_t dataSize) {
    // TODO
	AMCOM_PacketState *state = &(receiver->receivedPacketState);
	AMCOM_PacketHeader *header = &(receiver->receivedPacket.header);
	uint8_t *payload = receiver->receivedPacket.payload;

	for (size_t k = 0; k<dataSize; k++){
		uint8_t *local_data = ((uint8_t*)data)+k;
		
		switch (*state)
		{
		case AMCOM_PACKET_STATE_EMPTY :
			if (*local_data == 0xA1) *state = AMCOM_PACKET_STATE_GOT_SOP;
			header->sop = *local_data;
			break;

		case AMCOM_PACKET_STATE_GOT_SOP:
			*state = AMCOM_PACKET_STATE_GOT_TYPE;
			header->type = *local_data;
			break;

		case AMCOM_PACKET_STATE_GOT_TYPE:
			header->length = *local_data;
			if(header->length > 200) *state = AMCOM_PACKET_STATE_EMPTY;
			else *state = AMCOM_PACKET_STATE_GOT_LENGTH;
			break;

		case AMCOM_PACKET_STATE_GOT_LENGTH:
			header->crc = (*local_data & 0xff);
			*state = AMCOM_PACKET_STATE_GOT_CRC_LO;
			break;

		case AMCOM_PACKET_STATE_GOT_CRC_LO:
			header->crc = ((*local_data <<8 ) | header->crc);
			if(header->length == 0) {
				*state = AMCOM_PACKET_STATE_GOT_WHOLE_PACKET;
				k--;
			}
			else{
			*state = AMCOM_PACKET_STATE_GETTING_PAYLOAD;
			}
			break;

		case AMCOM_PACKET_STATE_GETTING_PAYLOAD:
			payload[receiver->payloadCounter] = *local_data;
			receiver->payloadCounter++;
			if(receiver->payloadCounter == header->length){
				*state = AMCOM_PACKET_STATE_GOT_WHOLE_PACKET;
				receiver->payloadCounter = 0;
				k--;
			}
			break;	

		case AMCOM_PACKET_STATE_GOT_WHOLE_PACKET: ;
			uint16_t local_crc = AMCOM_INITIAL_CRC;

			local_crc = AMCOM_UpdateCRC(header->type, local_crc);
			local_crc = AMCOM_UpdateCRC(header->length, local_crc);
			for (size_t t = 0; t< header->length; t++){
				local_crc = AMCOM_UpdateCRC(((char*)payload)[t],local_crc);
			}

			if(local_crc == (header->crc)){
				(receiver->packetHandler)(&(receiver->receivedPacket), receiver->userContext);
			}
			*state = AMCOM_PACKET_STATE_EMPTY;
			break;

		}
	}
}
