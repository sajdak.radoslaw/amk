#undef UNICODE
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <stdlib.h>
#include <stdio.h>
#include "math.h"

#include "amcom.h"
#include "amcom_packets.h"

#define DEFAULT_TCP_PORT 	"2001"
#define M_PI 3.14159265358979323846

static AMCOM_PlayerState players[AMCOM_MAX_PLAYER_UPDATES];
static AMCOM_FoodState foodState[AMCOM_MAX_FOOD_UPDATES];
static AMCOM_NewGameRequestPayload newGameRequestPayload;
static char playerName[AMCOM_MAX_PLAYER_NAME_LEN]= "Graczee2e";

typedef struct AMCOM_TargetPlayer{
	uint8_t targetPlayer;
	float targetPlayerX;
	float targetPlayerY;
	float distanceToClosestPlayer;
}AMCOM_TargetPlayer;

typedef struct AMCOM_TargetFood{
	uint16_t targetFood;
	float foodX;
	float foodY;
	float distanceToClosestFood;
}AMCOM_TargetFood;

float amcomCalculateDistance(float x_A, float y_A, float x_B, float y_B){
	float distance = sqrt(pow((x_B - x_A),2) + pow((y_B - y_A), 2));

	return distance;
}

AMCOM_TargetPlayer amcomFindTargetPlayer(){
	uint8_t currentPlayer = newGameRequestPayload.playerNumber;
	AMCOM_TargetPlayer targetPlayer;

	float tempDistance = 0;
	float targetDistance = newGameRequestPayload.mapWidth + newGameRequestPayload.mapHeight;
	for(uint8_t i = 0; i < AMCOM_MAX_PLAYER_UPDATES; i++){
		if(currentPlayer != i){
			tempDistance = amcomCalculateDistance(players[currentPlayer].x, players[currentPlayer].y, players[i].x, players[i].y);
			if((tempDistance < targetPlayer.distanceToClosestPlayer) && (players[currentPlayer].hp > players[i].hp)) {
				targetPlayer.distanceToClosestPlayer = tempDistance;
				targetPlayer.targetPlayer = players[i].playerNo;
				targetPlayer.targetPlayerX = players[i].x;
				targetPlayer.targetPlayerY = players[i].y;
			}
		}
	}
	return targetPlayer;
}

AMCOM_TargetFood amcomFindNearestFood(){
	//OUTPUT food ID
	float playerX = players[newGameRequestPayload.playerNumber].x;
	float playerY = players[newGameRequestPayload.playerNumber].y;
	AMCOM_TargetFood targetFood;

	uint16_t nearestFoodNo = 0;
	// Calculate the diagonal of map as the biggest distance
	float lastCalculatedDistance = 0;//amcomCalculateDistance(-1 * newGameRequestPayload.mapWidth / 2, newGameRequestPayload.mapHeight / 2, newGameRequestPayload.mapWidth / 2, -1 * newGameRequestPayload.mapHeight / 2);
	targetFood.distanceToClosestFood = newGameRequestPayload.mapHeight + newGameRequestPayload.mapWidth;

	for(uint16_t i = 0; i < AMCOM_MAX_FOOD_UPDATES; i++){
		if(foodState[i].state==1){
			lastCalculatedDistance = amcomCalculateDistance(playerX, playerY, foodState[i].x, foodState[i].y);
			if(targetFood.distanceToClosestFood > lastCalculatedDistance){
				targetFood.distanceToClosestFood = lastCalculatedDistance;
				targetFood.targetFood = foodState[i].foodNo;
				targetFood.foodX = foodState[i].x;
				targetFood.foodY = foodState[i].y;
			}
		}
	}
	return targetFood;
}

float amcomGetDirection(float destinationX, float destinationY){
	float playerX = players[newGameRequestPayload.playerNumber].x;
	float playerY = players[newGameRequestPayload.playerNumber].y;
	float angle = atan((destinationY - playerY)/(destinationX - playerX));// * (M_PI / 180.0);
	if (playerX > destinationX) angle += M_PI;
	return angle;
}
/**
 * This function will be called each time a valid AMCOM packet is received
 */
void amcomPacketHandler(const AMCOM_Packet* packet, void* userContext) {
	uint8_t amcomBuf[AMCOM_MAX_PACKET_SIZE];	// buffer used to serialize outgoing packets
	size_t bytesToSend = 0;						// size of the outgoing packet
	static int playerCounter;					// just a counter to distinguish player instances
	SOCKET sock = (SOCKET)userContext;			// socket used for communication with the game client

	switch (packet->header.type) {
	case AMCOM_IDENTIFY_REQUEST:
		printf("Got IDENTIFY.request. Responding with IDENTIFY.responseeeeee\n");
		AMCOM_IdentifyResponsePayload identifyResponse;
		sprintf(identifyResponse.playerName, playerName);
		bytesToSend = AMCOM_Serialize(AMCOM_IDENTIFY_RESPONSE, &identifyResponse, sizeof(identifyResponse), amcomBuf);
		break;
	case AMCOM_NEW_GAME_REQUEST:
		printf("Got NEW_GAME.request.\n");
		// TODO: respond with NEW_GAME.confirmation
		
		newGameRequestPayload.playerNumber = ((AMCOM_NewGameRequestPayload*)packet->payload)->playerNumber;
		newGameRequestPayload.numberOfPlayers = ((AMCOM_NewGameRequestPayload*)packet->payload)->numberOfPlayers;
		newGameRequestPayload.mapWidth = ((AMCOM_NewGameRequestPayload*)packet->payload)->mapWidth;
		newGameRequestPayload.mapHeight = ((AMCOM_NewGameRequestPayload*)packet->payload)->mapHeight;
		memset(players, 0, sizeof(players));
		memset(foodState, 0, sizeof(foodState));
		playerCounter = 0;

		bytesToSend = AMCOM_Serialize(AMCOM_NEW_GAME_RESPONSE,NULL,0,amcomBuf);

	    break;
	case AMCOM_PLAYER_UPDATE_REQUEST:
		printf("Got PLAYER_UPDATE.request.\n");
		// TODO: use the received information
		AMCOM_PlayerUpdateRequestPayload* playerUpdateRequestPayload = (AMCOM_PlayerUpdateRequestPayload*)packet->payload;
		for(uint8_t i = 0; i < AMCOM_MAX_PLAYER_UPDATES; i++){
			for(uint8_t j = 0; j < AMCOM_MAX_PLAYER_UPDATES; j++){
				if( players[i].playerNo == playerUpdateRequestPayload->playerState[j].playerNo){
					players[i].hp = playerUpdateRequestPayload->playerState[j].hp;
					players[i].x = playerUpdateRequestPayload->playerState[j].x;
					players[i].y = playerUpdateRequestPayload->playerState[j].y;
				}
			}
		}
	    break;
	case AMCOM_FOOD_UPDATE_REQUEST:
		printf("Got FOOD_UPDATE.request.\n");
		// TODO: use the received information
		
		AMCOM_FoodUpdateRequestPayload* foodUpdateRequestPayload = (AMCOM_FoodUpdateRequestPayload*)packet->payload;

		for(uint8_t i = 0; i < AMCOM_MAX_FOOD_UPDATES; i++){//iterate over incomming message

			if (foodUpdateRequestPayload->foodState[i].state == 1){//new food appeared
				for(uint8_t j = 0; j < AMCOM_MAX_FOOD_UPDATES; j++){//add to tab on empty position
					if(foodState[j].state == 0){
						foodState[j].foodNo = foodUpdateRequestPayload->foodState[i].foodNo;
						foodState[j].state = foodUpdateRequestPayload->foodState[i].state;
						foodState[j].x = foodUpdateRequestPayload->foodState[i].x;
						foodState[j].y = foodUpdateRequestPayload->foodState[i].y;
						printf("ADDED");
						break; //new item added
					}
				}
			}
			else {//food consumed or empty value
				for(uint8_t j = 0; j < AMCOM_MAX_FOOD_UPDATES; j++){//remove from tab
					if(foodState[j].foodNo == foodUpdateRequestPayload->foodState[i].foodNo){
						foodState[j].state = 0;
						printf("REMOVED");
						break; //new item added
					}
				}
			}

			printf("fooood: %d, %d, X:%f, Y:%f\n", foodState[i].foodNo, foodState[i].state, foodState[i].x, foodState[i].y);

		}

		memset(foodUpdateRequestPayload, 0, sizeof(AMCOM_FoodUpdateRequestPayload));

		break;
	case AMCOM_MOVE_REQUEST:
		printf("Got MOVE.request.\n");
		// TODO: respond with MOVE.confirmation
		AMCOM_MoveRequestPayload* moveRequestPayload = (AMCOM_MoveRequestPayload *)packet->payload;
		players[newGameRequestPayload.playerNumber].x = moveRequestPayload->x;
		players[newGameRequestPayload.playerNumber].y = moveRequestPayload->y;

		// Decision tree
		AMCOM_MoveResponsePayload moveResponsePayload;
		AMCOM_TargetFood targetFood = amcomFindNearestFood();
		AMCOM_TargetPlayer targetPlayer = amcomFindTargetPlayer();

		if(targetFood.distanceToClosestFood < targetPlayer.distanceToClosestPlayer){
			moveResponsePayload.angle = amcomGetDirection(targetFood.foodX, targetFood.foodY);
		}else{
			moveResponsePayload.angle = amcomGetDirection(targetPlayer.targetPlayerX, targetPlayer.targetPlayerY);
		}	
		
		//printf("Position X: %f, Y: %f\n", players[newGameRequestPayload.playerNumber].x, players[newGameRequestPayload.playerNumber].y);
		//printf("NearestFood ID: %d, X: %f, Y: %f\n", nearestFood, targetX, targetY);
		
		//printf("Direction: %f", amcomGetDirection(targetX, targetY)* (180.0/3.14));
		bytesToSend = AMCOM_Serialize(AMCOM_MOVE_RESPONSE,&moveResponsePayload, sizeof(moveResponsePayload), amcomBuf);
		break;
	}

	if (bytesToSend > 0) {
		int bytesSent = send(sock, (const char*)amcomBuf, bytesToSend, 0 );
		if (bytesSent == SOCKET_ERROR) {
			printf("Socket send failed with error: %d\n", WSAGetLastError());
			closesocket(sock);
			return;
		} else {
			printf("Sent %d bytes through socket.\n", bytesSent);
		}
	}
}

DWORD WINAPI playerThread( LPVOID lpParam )
{
	AMCOM_Receiver amcomReceiver;		// AMCOM receiver structure
	SOCKET sock = (SOCKET)(lpParam);	// socket used for communication with game client
	char buf[512];						// buffer for temporary data
	int receivedBytesCount;				// holds the number of bytes received via socket

	printf("Got new TCP connection.\n");

	// Initialize AMCOM receiver
	AMCOM_InitReceiver(&amcomReceiver, amcomPacketHandler, (void*)sock);

	// Receive data from socket until the peer shuts down the connection
	do {
		// Fetch the bytes from socket into buf
		receivedBytesCount = recv(sock, buf, sizeof(buf), 0);
		if (receivedBytesCount > 0) {
			printf("Received %d bytes in socket.\n", receivedBytesCount);
			// Try to deserialize the incoming data
			AMCOM_Deserialize(&amcomReceiver, buf, receivedBytesCount);
		} else if (receivedBytesCount < 0) {
			// Negative result indicates that there was socket communication error
			printf("Socket recv failed with error: %d\n", WSAGetLastError());
			closesocket(sock);
			break;
		}
	} while (receivedBytesCount > 0);

	printf("Closing connection.\n");

	// shutdown the connection since we're done
	receivedBytesCount = shutdown(sock, SD_SEND);
	// cleanup
	closesocket(sock);

	return 0;
}

int main(int argc, char **argv) {
	WSADATA wsaData;						// socket library data
	SOCKET listenSocket = INVALID_SOCKET;	// socket on which we will listen for incoming connections
	SOCKET clientSocket = INVALID_SOCKET;	// socket for actual communication with the game client
	struct addrinfo *addrResult = NULL;
	struct addrinfo hints;
	int result;

	// Say hello
	printf("mniAM player listening on portSSK %s\nPress CTRL+x to quit\n", DEFAULT_TCP_PORT);

	// Initialize Winsock
	result = WSAStartup(MAKEWORD(2,2), &wsaData);
	if (result != 0) {
		printf("WSAStartup failed with error: %d\n", result);
		return -1;
	}

	// Prepare hints structure
	memset(&hints, 0, sizeof(hints));
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;
	hints.ai_flags = AI_PASSIVE;

	// Resolve the server address and port
	result = getaddrinfo(NULL, DEFAULT_TCP_PORT, &hints, &addrResult);
	if ( result != 0 ) {
		printf("Function 'getaddrinfo' failed with error: %d\n", result);
		WSACleanup();
		return -2;
	}

	// Create a socket for connecting to server
	listenSocket = socket(addrResult->ai_family, addrResult->ai_socktype, addrResult->ai_protocol);
	if (listenSocket == INVALID_SOCKET) {
		printf("Function 'socket' failed with error: %ld\n", WSAGetLastError());
		freeaddrinfo(addrResult);
		WSACleanup();
		return -3;
	}
	// Setup the TCP listening socket
	result = bind(listenSocket, addrResult->ai_addr, (int)addrResult->ai_addrlen);
	if (result == SOCKET_ERROR) {
		printf("Function 'bind' failed with error: %d\n", WSAGetLastError());
		freeaddrinfo(addrResult);
		closesocket(listenSocket);
		WSACleanup();
		return -4;
	}
	freeaddrinfo(addrResult);

	// Listen for connections
	result = listen(listenSocket, SOMAXCONN);
	if (result == SOCKET_ERROR) {
		printf("Function 'listen' failed with error: %d\n", WSAGetLastError());
		closesocket(listenSocket);
		WSACleanup();
		return -5;
	}

	while (1) {
		// Accept client socket
		clientSocket = accept(listenSocket, NULL, NULL);
		if (clientSocket == INVALID_SOCKET) {
			printf("Function 'accept' failed with error: %d\n", WSAGetLastError());
			closesocket(listenSocket);
			WSACleanup();
			return -6;
		} else {
			// Run a separate thread to handle the actual game communication
			CreateThread(NULL, 0, playerThread, (void*)clientSocket, 0, NULL);
		}
		Sleep(10);
	}

	// No longer need server socket
	closesocket(listenSocket);
	// Deinitialize socket library
	WSACleanup();
	// We're done
	return 0;
}
